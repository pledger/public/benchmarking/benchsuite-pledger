#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from typing import ClassVar, Optional, Any

from pydantic import BaseModel, validator, Field

from benchsuite.core.cli.cli_config import human_time_to_seconds, comma_separated_list
from benchsuite.core.config import DBConfig, MetricsDBConfig, BaseBenchsuiteConfig


class KafkaConfig(BaseModel):
    enabled: bool = True
    bootstrap_servers: str = ''
    security_protocol: str = 'SSL'
    ssl_ca_file: str = '/kafka-auth/cacert.pem'
    ssl_cert_file: str = '/kafka-auth/client_cert.pem'
    ssl_key_file: str = '/kafka-auth/client_key.pem'
    ssl_verify_hostname: bool = True


class ConfserviceConfig(BaseModel):
    endpoint: str = ''
    username: str = ''
    password: str = ''


class ModelSynchronizerConfig(BaseModel):
    enabled: bool = True
    kafka_topic: str = 'configuration'
    interval: str = Field('10m', click={'help': 'interval for model synchronization (only in case kafka is disabled)'})
    set_nodes_to_test: bool = True
    kubernetes_profiler: str = Field('k8s-docker-stats', click={'help': 'can be k8s-docker-stats or prometheus-kubernetes'})
    workloads: str = []
    test_interval_days: int = Field('1', click={'help': 'interval for test execution in days (schedule frequency)'})

    # validators
    _convert_interval = validator('interval', allow_reuse=True)(human_time_to_seconds)
    _convert_workloads = validator('workloads', allow_reuse=True)(comma_separated_list)


class AnalyticsConfig(BaseModel):
    enabled: bool = True
    kafka_topic: str = 'benchmarking'
    send_only_performance_index: bool = False
    run_interval: str = '12h'
    calculation_period: str = '30d'
    categories_from_mongodb: bool = True

    # validators
    _convert_interval = validator('run_interval', 'calculation_period', pre=True, always=True, allow_reuse=True)(human_time_to_seconds)


class KeycloakConfig(BaseModel):
    auth_url: str = "localhost"
    realm: str = "master"
    client_id: str = None
    client_secret: str = None
    user: str = None
    user_password: str = None


class Config(BaseBenchsuiteConfig):

    verbosity: int = 2
    disabled_loggers: str = ""
    dry_run: bool = False

    db: DBConfig
    confservice: ConfserviceConfig
    kafka: KafkaConfig
    metrics_db: MetricsDBConfig
    analytics_db: MetricsDBConfig
    synchronizer: ModelSynchronizerConfig
    analytics: AnalyticsConfig
    keycloak: KeycloakConfig

    _convert_loggers = validator('disabled_loggers', allow_reuse=True)(comma_separated_list)
