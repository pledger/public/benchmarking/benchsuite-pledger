#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import datetime
import logging
import uuid
import traceback

from kafka import KafkaConsumer
from prettytable import PrettyTable
from prometheus_client import Summary, Histogram, Gauge
from prometheus_client.utils import INF

from benchsuite.core.db.configuration_db import BenchsuiteConfigurationDB
from benchsuite.core.model.schedule import Schedule
from benchsuite.pledger.config import Config
from benchsuite.pledger.confservice import ConfService
import time

from benchsuite.stdlib.providers.docker.model import DockerProviderModel
from benchsuite.stdlib.providers.holo import HoloProviderModel, HOLOVMModel
from benchsuite.stdlib.providers.kubernetes.model import KubernetesProviderModel
from benchsuite.stdlib.providers.model import ProviderSSHTunnelingModel

logger = logging.getLogger(__name__)

PLEDGER_MODEL_SYNCHRONIZER_UP = Gauge('benchsuite_pledger_model_sync_up', 'Benchsuite Pledger model synchronizer is up')
PLEDGER_MODEL_SYNC = Histogram('benchsuite_pledger_model_sync', 'Time spent syncrhonizing Pledger model',
                               buckets=[0, 10, 10.5, 11, 11.5, 12, 12.5, 13, 13.5, 14, 14.5, 16, 16.5, 17, 17.5, 18, 18.5, 19, 19.5, 20, 20.5, 21, 21.5, 22, INF])

PLEDGER_MODEL_SYNCHRONIZER_UP.set(0)


class BenchsuiteForPledger(BenchsuiteConfigurationDB):

    def list_syncd_providers(self):
        return self.list_providers(filters={'metadata.pledger-project': { '$exists': True }})

    def list_syncd_schedules(self):
        return self.list_schedules(filters={'metadata.pledger-project': { '$exists': True }})


class Synchronizer(object):

    def __init__(self, config: Config):
        self.config = config
        self.pledger = ConfService(self.config.confservice)
        self.benchsuite = BenchsuiteForPledger(**self.config.flatten_dict())

    def start(self):

        PLEDGER_MODEL_SYNCHRONIZER_UP.set(1)

        if not self.config.synchronizer.kafka_topic and self.config.synchronizer.interval:
            logger.info('Starting time-based synchronization because kafka_topic is not configured (sync every %ss)', self.config.synchronizer.interval)
            while True:
                try:
                    self._sync()
                except Exception as ex:
                    logger.error('Got an unhandled exception running the model synchronization calculator: %s', str(ex))
                finally:
                    time.sleep(self.config.synchronizer.interval)

        logger.info('Starting kafka-based synchronization on topic %s', self.config.synchronizer.kafka_topic)

        consumer = KafkaConsumer(
            self.config.synchronizer.kafka_topic,
            bootstrap_servers=self.config.kafka.bootstrap_servers.split(','),
            security_protocol=self.config.kafka.security_protocol,
            ssl_check_hostname=self.config.kafka.ssl_verify_hostname,
            ssl_cafile=self.config.kafka.ssl_ca_file,
            ssl_certfile=self.config.kafka.ssl_cert_file,
            ssl_keyfile=self.config.kafka.ssl_key_file,
            api_version=(2, 5, 0))

        for msg in consumer:
            logger.info('Received kafka message on topic %s: %s', self.config.synchronizer.kafka_topic, msg)
            try:
                self._sync()
            except Exception as ex:
                logger.error('Got an unhandled exception running the model synchronizer: %s', str(ex))
                logger.error(traceback.format_exc())

    @PLEDGER_MODEL_SYNC.time()
    def _sync(self):
        logger.debug('--- STARTING SYNCHRONIZATION ---')

        report = {'new_prov': [], 'mod_prov': [], 'del_prov': [], 'new_sch': [], 'mod_sch': [], 'del_sch': []}

        # 1. get Pledger infrastructure accounts, infrastructures and nodes
        plg_model = self.pledger.get_model()
        self._print_plg_models(plg_model)

        for sp in plg_model.service_providers:

            logger.debug(f'### User {sp.name}: ###')

            bes_client = self.benchsuite.as_user(sp.name)

            plg_projects = sp.projects
            bes_providers = bes_client.list_syncd_providers()
            bes_schedules = bes_client.list_syncd_schedules()
            bes_workloads = bes_client.list_workloads()

            bes_touched_providers = []
            bes_touched_schedules = []

            for p in plg_projects:

                try:
                    if p.infrastructure.type == 'K8S' or p.infrastructure.type == 'soe':
                        new_prov = self._project_to_k8s_provider(p)
                    elif p.infrastructure.type == 'Docker':
                        new_prov = self._project_to_docker_provider(p)
                    elif p.infrastructure.type == 'custom' and p.infrastructure.name == 'UC1-edge':
                        new_prov = self._project_to_holo_provider(p)
                    else:
                        logger.warning('  Skipping project %s because the infrastructure "%s" is not supported', p.name,
                                       p.infrastructure.type)
                        continue
                    old_prov = [prov for prov in bes_providers if str(prov.metadata.get('pledger-project', None)) == str(p.id)]
                    old_prov = old_prov[0] if old_prov else None # select first match

                    if not old_prov:
                        logger.debug('  [+] Creating benchsuite provider with id=%s for pledger project %s', new_prov.id, p.id)
                        report['new_prov'].append(new_prov)

                        if not self.config.dry_run:
                            bes_client.add_provider(new_prov)
                        else:
                            logger.warn('Skipping because dry-run flag is set')
                    else:
                        # provider already exists. Update it
                        logger.debug('  [~] Pledger project with id=%s already present in the benchsuite as provider with id=%s',
                                     p.id, old_prov.id)
                        new_prov.id = old_prov.id
                        report['mod_prov'].append(new_prov)
                        if not self.config.dry_run:
                            bes_client.update_provider(new_prov)
                        else:
                            logger.warn('Skipping because dry-run flag is set')

                        # remember that this provider has been synched
                        bes_touched_providers.append(old_prov.id)

                    new_sched = self._create_schedule(p, new_prov, bes_client, sp, bes_workloads)
                    old_sched = [sched for sched in bes_schedules if str(sched.metadata.get('pledger-project', None)) == str(p.id)]
                    old_sched = old_sched[0] if old_sched else None  # select first match

                    if not old_sched:
                        logger.debug('  [+] Creating benchsuite schedule with id=%s for pledger project %s', new_sched.id, p.id)
                        report['new_sch'].append(new_sched)
                        if not self.config.dry_run:
                            bes_client.add_schedule(new_sched)
                        else:
                            logger.warn('Skipping because dry-run flag is set')
                    else:
                        logger.debug('  [~] Schedule for Pledger project with id=%s already present in the benchsuite as '
                                     'schedule with id=%s', p.id, old_sched.id)
                        new_sched.id = old_sched.id

                        # (to keep scheduling hints modified manually)
                        new_sched.scheduling_hints = old_sched.scheduling_hints

                        report['mod_sch'].append(new_sched)

                        #new_sched.active = old_sched.active
                        if not self.config.dry_run:
                            bes_client.update_schedule(new_sched)
                        else:
                            logger.warn('Skipping because dry-run flag is set')

                        # remember that this schedule has been synced
                        bes_touched_schedules.append(old_sched.id)
                except:
                    traceback.print_exc()
                    logger.error(f'  Got an exception during sync for pledger project "{p.id}".')

            try:
                providers_to_remove = [p for p in bes_providers if p.id not in bes_touched_providers]
                schedules_to_remove = [s for s in bes_schedules if s.id not in bes_touched_schedules]

                for p in providers_to_remove:
                    logger.info(f'  [-] Removing provider "{p.name}" because not present in Pledger')
                    report['del_prov'].append(p)

                    if not self.config.dry_run:
                        bes_client.delete_provider(p)
                    else:
                        logger.warn('Skipping because dry-run flag is set')

                for s in schedules_to_remove:
                    logger.info(f'  [-] Removing schedule "{s.name}" because not present in Pledger')
                    report['del_sch'].append(s)

                    if not self.config.dry_run:
                        bes_client.delete_schedule(s)
                    else:
                        logger.warn('Skipping because dry-run flag is set')
            except:
                traceback.print_exc()
                logger.error(f'  Got an exception during removal of deleted Pledger projects')

            logger.debug('--- SYNCHRONIZATION FINISHED ---')
            logger.debug('')

        self._print_report(report)

    def _print_report(self, report):
        table = PrettyTable()
        table.field_names = ["User", "Provider", "Action"]
        table.align = 'l'
        for r in report['new_prov']:
            table.add_row([r.grants['owner'][4:], r.name, 'NEW'])
        for r in report['mod_prov']:
            table.add_row([r.grants['owner'][4:], r.name, 'MOD'])
        for r in report['del_prov']:
            table.add_row([r.grants['owner'][4:], r.name, 'DEL'])

        print('\n'+table.get_string())

        table = PrettyTable()
        table.field_names = ["User", "Scheduler", "Scope", "Active", "Action"]
        table.align = 'l'
        table.align['Active'] = 'c'
        for r in report['new_sch']:
            table.add_row([r.username, r.name, r.reports_scope[4:], "✔" if r.active else "", 'NEW'])
        for r in report['mod_sch']:
            table.add_row([r.username, r.name, r.reports_scope[4:], "✔" if r.active else "", 'MOD'])
        for r in report['del_sch']:
            table.add_row([r.username, r.name, r.reports_scope[4:], "", 'DEL'])

        print('\n'+table.get_string())

    def _create_schedule(self, project, prov, dao, service_provider, all_visible_workloads):
        sched = Schedule()
        sched.id = str(uuid.uuid4())
        sched.name = f'Testing-{project.name}-{project.infrastructure.name}'
        sched.scheduling_hints.interval = {
            'days': self.config.synchronizer.test_interval_days
        }
        sched.scheduling_hints.before = datetime.time(23, 59, 59)
        sched.scheduling_hints.after = datetime.time(0, 0, 0)
        sched.active = project.enableBenchmark
        sched.username = project.serviceProvider.name
        sched.provider_id = prov.id
        sched.metadata['pledger-project'] = str(project.id)
        if not project.privateBenchmark:
            sched.reports_scope = "org:public"
        else:
            sched.reports_scope = "usr:"+service_provider.name
        shed_tests = []
        for id_or_wid in self.config.synchronizer.workloads:
            shed_tests.extend([w for w in all_visible_workloads if hasattr(w, 'id') and w.id == id_or_wid])
            shed_tests.extend(self.filter_by_wid(all_visible_workloads, id_or_wid))
        sched.tests = [w.id for w in shed_tests]
        return sched

    def _project_to_holo_provider(self, project):

        assert 'tunnelPassword' in project.credentials, 'Invalid provider model: "tunnelPassword" not specified in "credentials" field'
        assert 'vmPassword' in project.credentials, 'Invalid provider model: "vmPassword" not specified in "credentials" field'

        model = HoloProviderModel(
            id=str(uuid.uuid4()),
            name=f'{project.name}-{project.infrastructure.name}',
            api_url='http://10.9.0.11:3000',
            execution_params_combination={'n_cpu': [2, 4], 'n_mem': [2, 4]},
            vm=HOLOVMModel(
                ip="192.168.109.8",
                user="pledger",
                platform="ubuntu",
                working_dir="/home/ubuntu",
                password=project.credentials['tunnelPassword'],
                ssh_port=22,
                ssh_tunnel=ProviderSSHTunnelingModel(
                    server='10.9.0.11',
                    user="nour",
                    password=project.credentials['vmPassword'],
                )
            )
        )

        model.grants["owner"] = "usr:" + project.serviceProvider.name
        model.description = 'Autogenerated by the benchsuite-pledger synchronizer'
        model.profiler = 'docker-stats'
        model.metadata['pledger-project'] = str(project.id)
        model.metadata['pledger-infrastructure'] = str(project.infrastructure.id)
        model.metadata['pledger-service-provider'] = str(project.serviceProvider.id)
        model.metadata['pledger-infrastructure-node-ids'] = {n.name: n.id for n in project.infrastructure.nodes}

        return model

    def _project_to_docker_provider(self, project):
        prov = DockerProviderModel(
            id=str(uuid.uuid4()),
            name=f'{project.name}-{project.infrastructure.name}',
            base_url=project.infrastructure.endpoint
        )
        prov.grants["owner"] = "usr:" + project.serviceProvider.name
        prov.description = 'Autogenerated by the benchsuite-pledger synchronizer'
        prov.profiler = 'docker-stats'
        prov.metadata['pledger-project'] = str(project.id)
        prov.metadata['pledger-infrastructure'] = str(project.infrastructure.id)
        prov.metadata['pledger-service-provider'] = str(project.serviceProvider.id)
        prov.metadata['pledger-infrastructure-node-ids'] = {n.name: n.id for n in project.infrastructure.nodes}

        return prov

    def _project_to_k8s_provider(self, project):

        assert 'namespace' in project.properties, 'Invalid provider model: "namespace" not specified in "properties" field.'

        prov = KubernetesProviderModel(
            id=str(uuid.uuid4()),
            name=f'{project.name}-{project.infrastructure.name}',
            api_server_url=project.infrastructure.endpoint,
            execution_params_combination={'namespace': project.properties['namespace']}
        )
        prov.grants["owner"] = "usr:" + project.serviceProvider.name
        prov.description = 'Autogenerated by the benchsuite-pledger synchronizer'
        prov.auth_method = 'userToken'
        prov.user_token = project.credentials.get('userToken', None)
        prov.api_server_cert = project.credentials.get('apiServerCert', None)
        prov.metadata['pledger-project'] = str(project.id)
        prov.metadata['pledger-infrastructure'] = str(project.infrastructure.id)
        prov.metadata['pledger-service-provider'] = str(project.serviceProvider.id)
        prov.metadata['pledger-infrastructure-node-ids'] = {n.name: n.id for n in project.infrastructure.nodes}

        if self.config.synchronizer.set_nodes_to_test:
            prov.execution_params_combination['k8s_node'] = [n.name for n in project.infrastructure.nodes if not n.properties['node_master']]

        if self.config.synchronizer.kubernetes_profiler == 'k8s-docker-stats':
            prov.profiler = 'k8s-docker-stats'
        elif self.config.synchronizer.kubernetes_profiler == 'prometheus-kubernetes':
            if project.infrastructure.prometheus_endpoint:
                prov.profiler = 'prometheus-kubernetes'
                prov.prometheus_endpoint = project.infrastructure.prometheus_endpoint

        return prov

    @staticmethod
    def filter_by_wid(workloads, wid):
        return [w for w in workloads if hasattr(w, 'bsid') and w.bsid.startswith(wid)]

    def _print_plg_models(self, plg_model):
        print('### PLEDGER ###')
        print('Users:')
        for p in plg_model.service_providers:
            print(f'\t{p.name}')
        print('Projects:')
        for p in plg_model.projects:
            print(f'\t{p.name}')
        print('Infrastructures:')
        for i in plg_model.infrastructures:
            print(f'\t{i.name}')