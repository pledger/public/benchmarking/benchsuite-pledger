#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import sys
import threading

import click

from benchsuite.core.cli.cli_config import options_from_model, options_from_model_2
from benchsuite.core.util.version import fmt_versions
from benchsuite.pledger.config import Config
from benchsuite.pledger.reports_publisher import BenchmarkReportsPublisher
from benchsuite.pledger.synchornizer import Synchronizer
from prometheus_client import start_http_server

from benchsuite.core.util.logging import init_logging

logger = logging.getLogger(__name__)


def synchronizer_thread(config):
    sy = Synchronizer(config)
    sy.start()


def analytics_thread(config):
    sy = BenchmarkReportsPublisher(config)
    sy.start()


@click.command()
@options_from_model(Config)
@options_from_model_2(Config)
@click.pass_context
def cli(ctx, config, **kwargs):

    print(fmt_versions()+"\n")
    config.pretty_print()

    init_logging(config.verbosity, config.disabled_loggers)

    if config.synchronizer.enabled:
        x = threading.Thread(target=synchronizer_thread, args=[config])
        x.start()

    if config.analytics.enabled:
        y = threading.Thread(target=analytics_thread, args=[config])
        y.start()

    start_http_server(9100)

    if config.synchronizer.enabled:
        x.join()

    if config.analytics.enabled:
        y.join()


def main(args=None):
    cli(auto_envvar_prefix='BPLG', args=args)


if __name__ == '__main__':
    main(args=sys.argv[1:])
