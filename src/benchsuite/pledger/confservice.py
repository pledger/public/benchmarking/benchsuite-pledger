#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import json
import time
import logging
from types import SimpleNamespace

import requests

logger = logging.getLogger(__name__)


class ConfService:

    def __init__(self, config):
        self.config = config
        self.__session = None
        self.__session_created_timestamp = 0
        self._model = None
        logger.info('Using Pledger Configuration Service at %s with user %s', config.endpoint, config.username)

    @staticmethod
    def __parse_json_str(string):
        # sanitize json strings since they are not in standard Json format
        return json.loads(string.replace('\'', '"').replace('"false"', "false").replace('"true"', "true"))

    def get_model(self):
        """
        Get objects from Confservice and adapt them to the internal model
        :return:
        """
        projects = self._get_list('/projects')
        infrastructures = self._get_list('/infrastructures')
        nodes = self._get_list('/nodes')
        service_providers = self._get_list('/service-providers')

        for n in nodes:
            res = json.loads(n.totalResources.replace('\'', '"')) if n.totalResources else {}
            cpu = int(int(res['cpu_millicore']) / 1000) if 'cpu_millicore' in res else 0
            mem = round(int(res['memory_mb']) / 1024, 2) if 'memory_mb' in res else 0
            n.resources = {'cpu': cpu, 'memory': mem}
            n.properties = self.__parse_json_str(n.properties) if n.properties else {}

        for i in infrastructures:
            if i.monitoringPlugin:
                i.monitoringPlugin = json.loads(i.monitoringPlugin.replace("'", '"'))
                i.prometheus_endpoint = i.monitoringPlugin.get('prometheus_endpoint', None)
            else:
                i.prometheus_endpoint = None
            i.nodes = [n for n in nodes if n.infrastructure.id == i.id]

        for p in projects:
            logger.info(f'Parsing project {p.name}...')
            if p.serviceProvider:
                p.serviceProvider = [s for s in service_providers if s.id == p.serviceProvider.id][0]
            if p.infrastructure:
                p.infrastructure = [i for i in infrastructures if i.id == p.infrastructure.id][0]
            if p.properties:
                try:
                    p.properties = json.loads(p.properties.replace("'", '"'))
                except Exception:
                    p.properties = None
                    logger.error('Unexpected content in "properties" for project %s', p.id)
            if p.credentials:
                try:
                    p.credentials = json.loads(p.credentials.replace("'", '"'))
                except Exception:
                    logger.error('Error converting "credentials" field in json for project %s', p.id)
                    p.credentials = {'userToken': p.credentials}
                    logger.debug('credentials is a string: assuming it corresponds to the user token')

            if not p.credentials:
                logger.warning('Credentials not found in "credentials" field. Assuming it is in the "properties" field')
                p.credentials = p.properties['credentials'] if (p.properties) and ('credentials' in p.properties) else {}

        for u in service_providers:
            u.projects = [p for p in projects if p.serviceProvider and p.serviceProvider.id == u.id]

        return SimpleNamespace(**{'service_providers': service_providers, 'projects': projects, 'infrastructures': infrastructures, 'nodes': nodes})

    def list_service_providers(self):
        return self._get_list('/service-providers')

    def list_benchmarking_reports(self):
        return self._get_list('/benchmark-reports')

    def get_benchmarking_report(self, id):
        return self._get(f'/benchmark-reports/{id}')

    def delete_benchmark_report(self, id):
        return self._delete(f'/benchmark-reports/{id}')

    def _get(self, resource):
        return self._request('GET', resource)['data']

    def _delete(self, resource):
        return self._request('DELETE', resource)['data']

    def _get_list(self, resource):
        return self._paginated_request('GET', resource)

    def _paginated_request(self, method, resource):
        page = 0
        size = 1000
        data = []

        while True:
            # do request
            res = self._request(method, resource, {'size': size, 'page': page, 'sort': 'id,asc'})
            if type(data) is not list:
                raise Exception(f'Expected list, but got a {type(res["data"])}: {res["data"]}')

            # append to results
            data.extend(res['data'])
            page = page + 1

            # if there is no next page stop
            if 'rel="next"' not in res['headers'].get('Link', ''):
                break

        logger.info('Fetched %s entities in %s requests', len(data), page)

        return data

    def _request(self, method, resource, params={}):
        session_age = time.time() - self.__session_created_timestamp
        if not self.__session or session_age >= (60 * 60 * 24):  # 1 day
            logger.debug("No session for the ConfService or session expired. Authenticating and getting token...")
            response = requests.post(self.config.endpoint + "/authenticate",
                                     json={'username': self.config.username,
                                           'password': self.config.password})
            token = response.json()['id_token']
            self.__session = requests.Session()
            self.__session_created_timestamp = time.time()
            self.__session.headers.update({'Authorization': f'Bearer {token}'})

        response = self.__session.request(method, self.config.endpoint + resource, params=params)
        logger.debug('Confservice response: %s', f'{response.status_code} {response.reason}')
        return {
            'headers': response.headers,
            'data': json.loads(response.text, object_hook=lambda d: SimpleNamespace(**d)) if response.text else None
        }
