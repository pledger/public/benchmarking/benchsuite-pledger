#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import datetime
import json
import logging
import time
import traceback
from collections import Counter
from pprint import pprint

import pytz
from influxdb import InfluxDBClient
from kafka import KafkaProducer
from prettytable import PrettyTable
from prometheus_client import Histogram, Gauge
from prometheus_client.utils import INF

from benchsuite.core.db.configuration_db import BenchsuiteConfigurationDB
from benchsuite.pledger.analytics import InfluxProxyDB, ANALYTICS_INTERVAL
from benchsuite.pledger.config import Config
from benchsuite.pledger.confservice import ConfService

logger = logging.getLogger(__name__)


BENCHMARKING_REPORTS_UP = Gauge('benchsuite_benchmarking_reports_up', 'Benchsuite Pledger reports generator is running')
BENCHMARKING_REPORTS_SYNC = Histogram('benchsuite_benchmarking_reports_calculation', 'Time spent creating benchmarking reports',
                                      buckets=[0, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, INF])

BENCHMARKING_REPORTS_UP.set(0)


class BenchmarkReportsPublisher(object):

    def __init__(self, config: Config):
        self.influx_proxy = InfluxProxyDB(config.metrics_db, config.keycloak)
        self.pledger = ConfService(config.confservice)

        self.config = config

        self.analytics_influx_client = InfluxDBClient(config.analytics_db.host, config.analytics_db.port,
                                            config.analytics_db.username, config.analytics_db.password)
        self.analytics_influx_client.switch_database(config.analytics_db.name)

        self.interval = int(config.analytics.run_interval)

        self.reportInterval = self.config.analytics.calculation_period

        self.producer = None
        if config.kafka.enabled and config.analytics.kafka_topic:
            self.producer = KafkaProducer(
                bootstrap_servers=config.kafka.bootstrap_servers.split(','),
                security_protocol=config.kafka.security_protocol,
                ssl_check_hostname=config.kafka.ssl_verify_hostname,
                ssl_cafile=config.kafka.ssl_ca_file,
                ssl_certfile=config.kafka.ssl_cert_file,
                ssl_keyfile=config.kafka.ssl_key_file,
                value_serializer=lambda v: json.dumps(v).encode('utf-8'),
                api_version=(2, 5, 0))
            self.kafka_topic = config.analytics.kafka_topic

        self.benchmarks_db_categories = {}

    def _update_categories_from_mongodb(self):
        if self.config.analytics.categories_from_mongodb:
            self.benchsuite = BenchsuiteConfigurationDB(**self.config.flatten_dict())
            res = self.benchsuite.as_user(requestor="").list_workloads()
            logger.debug('Found %s workloads', len(res))
            for w in res:
                if not hasattr(w, 'categories') or not w.categories:
                    continue
                if hasattr(w, 'tool'):
                    if w.tool not in self.benchmarks_db_categories:
                        self.benchmarks_db_categories[w.tool] = {}
                    if hasattr(w, 'name') and w.name not in self.benchmarks_db_categories[w.tool]:
                        self.benchmarks_db_categories[w.tool][w.name] = w.categories

                    # support metrics where "workload" tag is set from value of key workload_parameters.name in the db
                    #if 'name' in w.workload_parameters:
                    #    if w.workload_parameters['name'] not in self.benchmarks_db_categories[w.tool]:
                    #        self.benchmarks_db_categories[w.tool][w.workload_parameters['name']] = w.categories
            logger.info('Categories from mongodb updated')

    def start(self):

        BENCHMARKING_REPORTS_UP.set(1)

        while True:
            try:
                self.run()
            except Exception as ex:
                logger.error('Got an unhandled exception running the analytics calculator: %s', str(ex))
                logger.error(traceback.format_exc())
            next_run = datetime.datetime.now() + datetime.timedelta(seconds=self.interval)
            logger.info('Next analytics calculation will be in %s seconds (at %s)', self.interval, next_run)
            time.sleep(self.interval)

    def get_users(self):
        return [{'id': u.id, 'name': u.name} for u in self.pledger.list_service_providers()]

    def get_analytics(self, users):
        self._update_categories_from_mongodb()
        all_analytics = []
        for u in users:
            logger.info('\n\n|\n| Calculating analytics for user "%s"\n|\n', u)
            all_analytics.extend(self.influx_proxy.get_analytics(user=u, interval_s=self.reportInterval))
        return all_analytics

    def get_benchmark_categories(self, tool, workload):
        if tool in self.benchmarks_db_categories and workload in self.benchmarks_db_categories[tool]:
            return self.benchmarks_db_categories[tool][workload]
        else:
            return []

    def create_benchmarking_report(self, tool, user_id, user_name, metric_name, tags, analytics_metrics):

        return {
            'metric': metric_name,
            'tool': tags['workload.tool'],
            'workload': tags['workload.name'],
            'workloadId': tags["workload"],
            'pledgerInfrastructure': int(tags['pledger-infrastructure']),
            'pledgerServiceProvider': int(user_id),
            'pledgerProject': int(tags['pledger-project']),
            'pledgerNode': int(tags['pledger-infrastructure-node']),
            'mean': analytics_metrics.get('mean', None),
            'categories': self.get_benchmark_categories(tool, tags['workload.name']),
            # FIXME... if we do not have stddev the stability should be None, but currently the Confservice
            # raises an exception when a None is received
            'stabilityIndex': (1 - analytics_metrics['r_stddev']) if analytics_metrics.get('r_stddev', None) is not None else 0,
            'apEn': analytics_metrics['apen'],
            'stddev': analytics_metrics.get('stddev', None),
            'r_stddev': analytics_metrics.get('r_stddev', None),
            'n_samples': analytics_metrics.get('n_samples', None),
            'interval': self.reportInterval,
            #'node': tags['node'],
            'provider': tags['provider.default'],
            'user': user_name
        }

    @staticmethod
    def __table_footer(tbl, text, dc):
        res = f"{tbl._vertical_char} {text}{' ' * (tbl._widths[0] - len(text))} {tbl._vertical_char}"

        for idx, item in enumerate(tbl.field_names):
            if idx == 0:
                continue
            if not item in dc.keys():
                res += f"{' ' * (tbl._widths[idx] + 1)} {tbl._vertical_char}"
            else:
                res += f"{' ' * (tbl._widths[idx] - len(str(dc[item])))} {dc[item]} {tbl._vertical_char}"

        res += f"\n{tbl._hrule}"
        return res

    def __print_reports(self, benchmarking_reports, print_raw_objects=False):
        if print_raw_objects:
            for r in benchmarking_reports:
                pprint(r)
        table = PrettyTable()
        table.field_names = ["WorkloadId", "Metric", "Tags (*)", "Avg", "StabilityIndex", "ApEn", "Samples", "Categories"]
        table.align = 'l'
        for r in benchmarking_reports:
                tags = f'{r["pledgerInfrastructure"]},{r["pledgerNode"]},{r["pledgerProject"]},{r["pledgerServiceProvider"]}'
                table.add_row([r['workloadId'], r['metric'], tags, r['mean'], r['stabilityIndex'], r['apEn'], r['n_samples'], ','.join(r['categories'])])
        print('\n'+table.get_string())
        print('(*) pledgerInfrastructure,pledgerNode,pledgerProject,pledgerServiceProvider>\n')

        aggregated = []
        for r in benchmarking_reports:
            aggregated.append((r['user'], r['tool'], r['provider']))
        c = Counter(aggregated)
        table = PrettyTable()
        table.field_names = ["User", "Tool", "Provider", "Count"]
        table.align = 'l'
        table.align["Count"] = "r"
        distinct = set(aggregated)
        for i in sorted(distinct):
            table.add_row([i[0], i[1], i[2], c[i]])
        #table.add_row(['Tot.', '', '', len(benchmarking_reports)])
        print('\n'+table.get_string())
        print(self.__table_footer(table, "Total", {'Count': len(benchmarking_reports)}))
        print()

    def __insert_analytics_into_influx(self, measurement, tags, new_metrics, user):

        records = []
        for metric, fields in new_metrics.items():
            record = {
                "measurement": 'Analytics',
                "tags":  {k: v for k, v in tags.items() if not k.startswith('pledger-')},
                "time": datetime.datetime.fromtimestamp(time.time(), tz=pytz.utc)
            }
            # make sure new metrics will be visible to the user
            record['tags']['scope'] = f'usr:{user}'
            record['tags']['metric'] = metric
            record['tags']['interval'] = self.reportInterval
            record['fields'] = fields
            records.append(record)

        logger.debug('Storing analytics for %s: %s data points', measurement, len(records))

        # store records
        self.analytics_influx_client.write_points(records)

    @BENCHMARKING_REPORTS_SYNC.time()
    def run(self, users=None):

        if not users:
            users = self.get_users()

        a = self.get_analytics([u['name'] for u in users])

        if not self.config.dry_run:
            self.store_analytics(a)
        else:
            logger.debug('Skipping analytics saving because dry_run=True')

        reports = self.generate_benchmarking_reports(a, users)
        logger.info('Generated %s benchmarking reports', len(reports))

        if self.config.verbosity > 2:
            self.__print_reports(reports, print_raw_objects=False)

        if self.config.kafka.enabled:
            self.send_reports_to_kafka(reports)
        else:
            logger.warning("Skipping sending benchmarking reports because Kafka is disabled or topic not specified")

    def send_reports_to_kafka(self, benchmarking_reports):
        for m in benchmarking_reports:
            self.producer.send(self.kafka_topic, m)
        logger.debug('Producer finished')
        self.producer.flush()

    def generate_benchmarking_reports(self, analytics, users):
        benchmarking_reports = []
        useridmap = {u['name']: u['id'] for u in users}
        for measurement, user, tags, new_metrics in analytics:
            # skip creation of incomplete (from Pledger ConfService perspective) reports
            if not tags['pledger-project'] or not tags['pledger-infrastructure'] or \
                    not tags['pledger-service-provider'] or not tags['pledger-infrastructure-node']:
                logger.debug('Skipping creation of report for measurement %s, user %s, tags %s because it does not belong to pledger', measurement, user, tags)
                continue

            for metric_name, values in new_metrics.items():
                if self.config.analytics.send_only_performance_index and metric_name != 'performance_index':
                    continue
                benchmarking_reports.append(self.create_benchmarking_report(
                    measurement, useridmap[user], user, metric_name, tags, values))
        return benchmarking_reports

    def store_analytics(self, analytics):
        for measurement, user, tags, new_metrics in analytics:
            self.__insert_analytics_into_influx(measurement, tags, new_metrics, user)
