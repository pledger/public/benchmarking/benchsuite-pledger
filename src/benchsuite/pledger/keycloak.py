#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import time
import traceback

from keycloak import KeycloakAdmin, KeycloakOpenID

logger = logging.getLogger(__name__)


# TODO: taken from rest module. Find a place to not duplicate the code
class KeycloakClient:

    def __init__(self, config):

        self.server = config.auth_url
        self.realm = config.realm
        self.client = config.client_id
        self.secret = config.client_secret
        self.user = config.user
        self.user_password = config.user_password
        self._admin_client = None
        self._token_issue_time = None
        self._user_token = None
        self._user_token_issue_time = None

    def is_user_token_expired(self):
        if not self._user_token or not self._user_token_issue_time:
            return True
        token_age = time.time() - self._user_token_issue_time

        if token_age > self._user_token['expires_in']:
            logger.debug('Token expired %s seconds ago.',
                         int(token_age - self._user_token['expires_in']))
            return True

        return False

    def get_user_token(self):
        # if token is expired, re-init the client
        # NOTE: KeycloakAdmin has an auto_token_refresh feature, but a better investigation
        # on how to make it work is needed
        if self.is_user_token_expired():
            self._user_token = None

        if not self._user_token:
            keycloak_openid = KeycloakOpenID(server_url=self.server,
                                             client_id=self.client,
                                             realm_name=self.realm,
                                             client_secret_key=self.secret)

            if self.user and self.user_password:
                self._user_token = keycloak_openid.token(self.user, self.user_password)
                logger.debug('Authenticated with user %s. Token expires in %s seconds', self.user, self._user_token['expires_in'])
            else:
                # in this case  the client must be "confidential" and "Service Accounts" must be enabled
                self._user_token = keycloak_openid.token(grant_type=['client_credentials'])
                logger.debug('Authenticated with client credentials. Token expires in %s seconds', self._user_token['expires_in'])

            self._user_token_issue_time = time.time()

        return self._user_token['access_token']

    def get_admin_client(self):

        # if token is expired, re-init the client
        # NOTE: KeycloakAdmin has an auto_token_refresh feature, but a better investigation
        # on how to make it work is needed
        if self._admin_client:
            token_age = time.time() - self._token_issue_time
            if token_age > self._admin_client.token['expires_in']:
                logger.debug('Token expired %s seconds ago. Re-authenticating...', int(token_age - self._admin_client.token['expires_in']))
                self._admin_client = None

        if not self._admin_client:
            self._admin_client = KeycloakAdmin(
                server_url=self.server,
                realm_name=self.realm,
                client_id=self.client,
                client_secret_key=self.secret,
                verify=False
            )
            logger.debug('Authenticated. Token expires in %s seconds', self._admin_client.token['expires_in'])
            self._token_issue_time = time.time()
        return self._admin_client

    def get_users(self):
        try:
            return self.get_admin_client().get_users()
        except Exception as e:
            print(traceback.format_exc())
            return None

    def get_user(self, username):
        try:
            return self.get_admin_client().get_user(self.get_admin_client().get_user_id(username))
        except Exception as e:
            print(traceback.format_exc())
            return None

    def get_user_groups(self, username):
        try:
            return self.get_admin_client().get_user_groups(self.get_admin_client().get_user_id(username))
        except Exception as e:
            print(traceback.format_exc())
            return None
        
    def user_exists(self, username):
        return self.get_user(username) is not None
