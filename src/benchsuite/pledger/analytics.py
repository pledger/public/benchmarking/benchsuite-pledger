#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import logging
import traceback

import antropy as ant
import numpy as np
from influxdb import InfluxDBClient
from prometheus_client import Histogram
from prometheus_client.utils import INF
from scipy.stats import gmean, gstd, variation

from benchsuite.pledger.keycloak import KeycloakClient

ANALYTICS_CALCULATION = Histogram('benchsuite_analytics_calculation', 'Time spent calculating analytics',
                                  ["user", "measurement", "metric", "interval"],
                                  buckets=[0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10, INF])


ANALYTICS_INTERVAL = 60 * 60 * 24 * 7  # one week


logger = logging.getLogger(__name__)

MEASUREMENTS_TO_SKIP = ['Analytics', 'Profile']


class InfluxProxyDB(object):
    def __init__(self, config, keycloak_config=None):

        self.config = config
        self.keycloak = KeycloakClient(keycloak_config) if keycloak_config else None
        self.__client = None

    def _get_client(self):
        if self.config.auth_token:
            if self.__client:
                return self.__client
            else:
                self.__client = InfluxDBClient(self.config.host, self.config.port,
                                                  self.config.username or None, self.config.password or None,
                                                  headers={'Authorization': f'Bearer {self.config.auth_token}'})
                self.__client.switch_database(self.config.name)
                return self.__client

        if self.keycloak.is_user_token_expired() or self.__client is None:
            c = self.config.auth_token if self.config.auth_token else self.keycloak.get_user_token()
            self.__client = InfluxDBClient(self.config.host, self.config.port,
                                              self.config.username or None, self.config.password or None,
                                              headers={'Authorization': f'Bearer {c}'})
            self.__client.switch_database(self.config.name)
        return self.__client

    def close(self):
        if self.__client:
            self.__client.close

    # not used??
    #def get_workloads(self, user=None, time_from=None, time_to=None):
    #    time_clause = time_to
    #    if time_from and time_to:
    #        time_clause = f'WHERE time >= {time_to} - {time_from} '
    #    query = f'select count(performance_index) from /.*/ {time_clause}GROUP BY workload.tool, workload.name, workload.version'
    #    qres = self._get_client().query(query, params={'u': user} if user else {})
    #    res = []
    #    for m, tags in qres.keys():
    #        val = next(qres.get_points(m, tags))
    #        res.append({'workload': {'tool': tags['workload.tool'], 'workload': tags['workload.name'], 'version': tags['workload.version']}, 'count': val['count']})
    #    return res

    def get_users_by_measurement(self):
        res = {}
        query = 'SHOW TAG VALUES WITH KEY="scope"'
        qres = self._get_client().query(query, params={'disableEnrichment': 'true'})
        for measurement, tags in qres.keys():
            if measurement == 'Analytics' or measurement == 'Profile':
                continue
            if measurement not in res:
                res[measurement] = set()
            for p in qres.get_points(measurement, tags):
                res[measurement].add(p['value'][4:])
        return res

    def compute_analytics(self, values):
        n = len(values)
        mean = gmean(values) if n > 0 else None
        std = gstd(values) if n > 2 else None
        rstd = variation(values) if n > 2 else None
        apen = ant.app_entropy(values, order=2) if n > 2 else None
        return {'mean': mean, 'stddev': std, 'r_stddev': rstd, 'apen': apen, 'n_samples': n}

    def get_metric_names_by_measurement(self, user, measurement='/.*/'):
        res = {}
        query = f'SHOW FIELD KEYS FROM {measurement}'
        logger.debug('Executing query %s', query)
        qres = self._get_client().query(query, params={'u': user})
        for m, tags in qres.keys():
            if m in MEASUREMENTS_TO_SKIP:
                continue
            names = []
            for v in qres.get_points(m, tags):
                names.append(v['fieldKey'])
            res[m] = names
        return res

    def __to_arrays(self, metric_names, resultset_points_iterator):
        res = {m: [] for m in metric_names}
        for v in resultset_points_iterator:
            for mn in metric_names:
                if mn in v:
                    res[mn].append(v[mn])
        return res

    def get_analytics(self, user="", measurement='/.*/', metric='.*', interval_s=ANALYTICS_INTERVAL):
        """
        :param user:
        :param measurement:
        :param interval_s:
        :return: a list of pairs in the form (measurement, user, tags, analytics)
        """
        res = []
        if not measurement.startswith('/') and not measurement.endswith('/'):
            measurement = '"' + measurement + '"'
        if measurement == 'Profile' or measurement == 'Analytics':
            return None

        # in case measurement is a regex we could also remove Profile and Analytics measurements, but apparently it is
        # not possible using only regex:
        # https://community.influxdata.com/t/influxql-exclude-measurements-through-regex/18285

        metrics_by_measurement = self.get_metric_names_by_measurement(user=user)

        # we do not add the scope because we want to compute analytics on all metrics visible to the user
        group_by = ["workload", "execenv.default", "provider.default"]

        # add further tags just to have them in the resultset
        group_by.extend(["workload.name", "workload.tool", "workload.version", "execenv.default.type"])

        # we add Pledger specific tags just to have them in the result (they will not create any new tag)
        group_by.extend(["pledger-project", "pledger-infrastructure", "pledger-service-provider", "pledger-infrastructure-node"])

        group_by = [f'"{v}"' for v in group_by]

        query = f'SELECT /{metric}/ FROM {measurement} WHERE time >= now() - {interval_s}s GROUP BY {",".join(group_by)}'

        with ANALYTICS_CALCULATION.labels(user=user, measurement=measurement, metric=metric, interval=interval_s).time():
            logger.debug('Executing query %s', query)
            qres = self._get_client().query(query, params={'u': user})
            logger.debug('Resultset size: %s', len(qres.items()))
            for m, tags in qres.keys():

                if m in MEASUREMENTS_TO_SKIP:
                    continue

                # skip old style series
                if not tags['workload.name']:
                    continue

                new_metrics = {}
                series = self.__to_arrays(metrics_by_measurement[m], qres.get_points(m, tags))
                for metric, values in series.items():
                    if None in values:
                        logger.debug('There are None values in the array. Cannot evaluate analytics (series: %s:%s, '
                                     'metric: %s, values: %s', m, tags, metric, values)
                        continue

                    try:
                        new_metrics[metric] = self.compute_analytics(values)
                    except Exception as ex:
                        logger.error('Error calculating analytics: ', str(ex))
                        print(traceback.format_exc())

                res.append((m, user, tags, new_metrics))

        return res

