#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os
from setuptools import setup, find_packages

# import the VERSION from the source code
import sys
sys.path.append(os.getcwd() + '/src/benchsuite')
from pledger import VERSION

def build_version():
    base_version = '.'.join(map(str, VERSION))
    final_version = base_version
    print(os.environ)
    if 'CI_COMMIT_BRANCH' in os.environ and 'CI_BUILD_ID' in os.environ:
        final_version = '{0}.dev{1}+{2}'.format(base_version, os.environ['CI_BUILD_ID'], os.environ['CI_COMMIT_BRANCH'])
    return final_version

setup(
    name='benchsuite.pledger',
    version=build_version(),
    description='A synchronizer of data between the Benchsuite and the Pledger System',
    long_description=open(os.path.join(os.path.dirname(__file__), 'README.md')).read(),

    url='https://gitlab.res.eng.it/benchsuite/benchsuite-pledger',

    author='Gabriele Giammatteo',
    author_email='gabriele.giammatteo@eng.it',

    license='Apache',

    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: Information Technology',
        'Topic :: System :: Benchmark',
        'Topic :: Utilities',
        'Topic :: Software Development :: Testing',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python :: 3 :: Only',
        'Environment :: Console',
        'Operating System :: Unix',
        'Framework :: Flask'
    ],
    keywords='benchmarking cloud testing performance',

    packages=find_packages('src'),
    namespace_packages=['benchsuite'],
    package_dir={'': 'src'},
    entry_points={
        'console_scripts': ['benchsuite-pledger-synchronizer=benchsuite.pledger.cli:main'],
    },

    install_requires=['benchsuite.core', 'benchsuite.stdlib', 'environ-config', 'influxdb', 'kafka-python', 'prettytable', 'python-keycloak==0.24.0', 'scipy==1.7.3', 'antropy==0.1.4', 'prometheus-client==0.14.1']

)
